import React, { Component } from 'react';

import Post from '../../components/Post/Post';
import Layout from '../../components/Layout/Layout'
import './Blog.css';

var images = require.context('../../assets/images/', true, /\.(png|jpeg|jpg|svg)$/);
    

class Blog extends Component {
    
    state = {
        studentInfo: {
            Id:1,
            Photo:1,
            Name:"Rahul",
            Address:"Bangalore",
            City:"Bangalore",
            Phone:5
        }   
    }

    render () {
      let img_src=null;

      try{
      img_src = images(`./${this.state.studentInfo.Name}.jpeg`)
      }
      catch (error){}
      

      
      
      const list = Object.entries(this.state.studentInfo).map(([key,value])=>{
        return <Post key={key} string={key.toString().toUpperCase()} value={value.toString()}/>
      })

        return (
            <div>
                  <Layout source={img_src}>
                    {list}
                    </Layout>
                 </div>
            

        );
    }
}

export default Blog;