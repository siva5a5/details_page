import React from 'react';

import Classes from './Post.css';
const post = (props) => {

    return(
    <tr>
          <td className={Classes.td}>{props.string} </td>
          <td className={Classes.td}>{props.value}</td>
    </tr>
    )

};

export default post;