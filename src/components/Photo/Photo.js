import React from 'react';
import classes from  './Photo.css';

//let source =require('../../assets/images/Rahul.jpeg');

const Photo = (props) => (
    <article className={classes.Photo}>
        <img src={props.source} alt="Photo" />
        
        </article>
       
);


export default Photo;