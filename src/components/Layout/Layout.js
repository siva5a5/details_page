import React from 'react';

import Aux from '../../hoc/_Aux';
import classes from './Layout.css';
import Photo from '../Photo/Photo';

/*const setSource = (source) => {
   let source1 ='../../assets/images/'+source+'.jpeg';
   let source2=require('../../assets/images/Rahul.jpeg');
    console.log(source2);
  }
*/
const Layout = (props) => (
    <Aux>
            <h1 className={classes.h1}>Details Page</h1>
            <div><Photo source={props.source}/></div>
            <main className={classes.Content}>
            <table className={classes.bordered}>
                <tbody>
                    {props.children}
                    </tbody>
            </table>
            </main>
           </Aux>
)
export default Layout;